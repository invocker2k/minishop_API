package com.minishop.Controller;

import java.util.List;

import com.minishop.Model.DanhMucSanPham;
import com.minishop.Repository.DanhMucRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/dm")
public class danh_mucController {
    @Autowired
    DanhMucRepository danhMucR;

    @GetMapping
    public Page<DanhMucSanPham> AllDanhMuc(Pageable pageable ) {
        
        return danhMucR.findAll(pageable);
    }
    

}