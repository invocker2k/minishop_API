package com.minishop.Controller;

import java.util.List;

import com.minishop.Model.NhanVien;
import com.minishop.Repository.NhanVienRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/nv")
public class Get_NhanVien {
        @Autowired
        NhanVienRepository nhanVienR;

        @GetMapping
        public  List<NhanVien> getAllNhanVien(){
            return nhanVienR.findAll();
        }
}