package com.minishop.Controller;

import java.util.List;

import com.minishop.Model.SanPham;
import com.minishop.Repository.SanPhamRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/sp")
public class SanPhamController {
    @Autowired 
    SanPhamRepository sanPhamR;

    @GetMapping
    public List<SanPham> getAllSP(){
        return sanPhamR.findAll();
    }
}