package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Data
@Entity(name = "sanpham")
public class SanPham implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int masanpham;
	private String tensanpham;
	private String giatien;
	private String mota;
    private String hinhsanpham;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="madanhmuc")
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="madanhmuc")
    @JsonIdentityReference(alwaysAsId=true)
    //@JsonManagedReference
   // @JsonBackReference
	DanhMucSanPham lisSanPham;
}