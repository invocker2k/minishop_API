package com.minishop.Model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity(name = "hoadon")
@Data
 public class HoaDon implements Serializable{
         /**
         *
         */
         private static final long serialVersionUID = 1L;
         @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int mahoadon;
        private String tenkhachhang;
        private String sdt;
        private String noigiaohang;
        private boolean tinhtrang;
        private String ngaylap;
        private String hinhthucgiaohang;
        private String ghichu;
        @OneToMany(cascade = CascadeType.ALL)
        @JoinColumn(name = "mahoadon")
        @JsonManagedReference
        @JsonBackReference
        Set<ChiTietHoaDon> danhSachchitiethoaDon;
}