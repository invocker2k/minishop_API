package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
@Data
@Entity(name = "mausanpham")
public class MauSanpham implements Serializable{
	/**
	 */
	private static final long serialVersionUID = -5828822444473862614L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mamau;
	private String tenmau;
}