package com.minishop.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity(name = "danhmucsanpham")
public class DanhMucSanPham implements Serializable{
    /**
    *
    */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String madanhmuc;
	private String tendanhmuc;
	private String hinhdanhmuc;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "madanhmuc")
   // @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="masanpham")
   // @JsonIdentityReference(alwaysAsId=true)
    @JsonManagedReference
    @JsonBackReference
    private List<SanPham> listSanPham = new ArrayList<>();
    
}