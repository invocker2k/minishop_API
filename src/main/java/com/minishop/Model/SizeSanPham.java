package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name="sizesanpham")
public class SizeSanPham implements Serializable{
		/**
	*
	*/
	private static final long serialVersionUID = 1L;
	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int masize;
		private String size;
}