package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import lombok.Data;

@Entity(name="chitiethoadon")
@Data
public class ChiTietHoaDon implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 7899202840352636750L;

	@EmbeddedId
	ChiTietHoaDonId chiTietHoaDonId;
	

	private int soluong;
    private String giatien;
}