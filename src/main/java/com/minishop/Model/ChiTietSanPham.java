package com.minishop.Model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
@Data
@Entity(name = "chitietsanpham")
public class ChiTietSanPham implements Serializable{
		/**
	*
	*/
	private static final long serialVersionUID = 1L;

	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
         int machitietsanpham;
        
		@OneToOne(cascade = CascadeType.ALL)
		@JoinColumn(name="masanpham")
		@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="masanpham")
    	@JsonIdentityReference(alwaysAsId=true)
        private SanPham sanPham;
        
        @OneToOne(cascade = CascadeType.ALL)
		@JoinColumn(name="masize")
		@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="masanpham")
    	@JsonIdentityReference(alwaysAsId=true)
		private SizeSanPham sizeSanPham;
		
		@OneToOne(cascade = CascadeType.ALL)
		@JoinColumn(name="mamau")
		@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="masanpham")
    	@JsonIdentityReference(alwaysAsId=true)
		private MauSanpham mauSanPham;
		
		private int soluong;
		private String ngaynhap;
}